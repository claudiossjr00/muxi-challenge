#include <assert.h>

#include "stack.h"
#include "stack_even_remove.h"

int compare_stack(Stack* expected, Stack* given) {
    if (expected->head != given->head) return 0;
    while (!stackEmpty(given) && !stackEmpty(expected)) {
        int expectedVal = stackPop(expected);
        int givenVal = stackPop(given);
        if (expectedVal != givenVal) return 0;
    }
    return 1;
}

void test_empty_stack() {
    Stack* expected = stackNew();
    Stack* stack = stackNew();
    
    stackRemoveEven(stack);

    assert( compare_stack(expected, stack) );
    printf("test_empty_stack -- pass \n");
}

void test_one_even_elem() {
    Stack* expected = stackNew();
    
    Stack* stack = stackNew();
    stackPush(stack, 10);

    stackRemoveEven(stack);
    assert( compare_stack(expected, stack) );
    printf("test_one_even_elem -- pass \n");
}

void test_one_odd_elem() {
    Stack* expected = stackNew();
    stackPush(expected, 11);
    
    Stack* stack = stackNew();
    stackPush(stack, 11);

    stackRemoveEven(stack);

    assert( compare_stack(expected, stack) );
    
    printf("test_one_odd_elem -- pass \n");
}

void test_one_even_and_one_odd() {
    Stack* expected = stackNew();
    stackPush(expected, 11);
    
    Stack* stack = stackNew();
    stackPush(stack, 11);
    stackPush(stack, 10);

    stackRemoveEven(stack);

    assert( compare_stack(expected, stack) );
    
    printf("test_one_even_and_one_odd -- pass \n");
}

int main(int argc, char* argv[]) {
    test_empty_stack();
    test_one_even_elem();
    test_one_odd_elem();
    test_one_even_and_one_odd();
    return 0;
}
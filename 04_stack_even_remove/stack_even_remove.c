#include <stdio.h>


#include "stack.h"
#include "stack_even_remove.h"


void stackRemoveEven (Stack *stack) {
    if (stack == NULL) return;
    Stack* aux = stackNew();
    while (stackEmpty(stack) != 1) {
        int elem = stackPop(stack);
        if (elem % 2 != 0) {
            stackPush(aux, elem);
        }
    }
    while (stackEmpty(aux) != 1) {
        int elem = stackPop(aux);
        stackPush(stack, elem);
    }
    stackFree(aux);
}


#pragma once

#include <stdio.h>
#include <stdlib.h>

#define STACKLENGTH 10

typedef struct stack {
    int elem[STACKLENGTH];
    int head;
} Stack;

Stack *stackNew (void);
void stackFree (Stack *p);
void stackClear (Stack *p);
void stackPush (Stack *p, int elem);
int stackPop (Stack *p);
int stackEmpty (Stack *p);
void stackPrint(Stack *p);
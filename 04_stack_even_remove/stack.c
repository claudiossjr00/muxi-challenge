
#include "stack.h"


Stack* stackNew (void) {
    Stack* p = (Stack*) malloc( sizeof(Stack) );
    for (int i = 0; i < STACKLENGTH; i++) {
        p->elem[i] = -1;
    }
    p->head = -1;
    return p;
}

void stackFree (Stack *p) {
    free(p);
    p = NULL;
}

void stackClear (Stack *p) {
    p->head = 0;
}

void stackPush (Stack *p, int elem) {
    int head = p->head;
    if (head < 10) {
        p->head++;
        p->elem[p->head] = elem;
    }
}

int stackPop (Stack *p) {
    if (p->head >= 0) {
        int elem = p->elem[p->head];
        p->head--;
        return elem;
    }
    return -1;
}

int stackEmpty (Stack *p) {
    return p->head == -1;
}

void stackPrint(Stack *p) {
    printf("Print Stack -> ");
    for (int i = p->head; i >= 0; i--) {
        printf("%d ", p->elem[i]);
    }
    printf("\n");
}
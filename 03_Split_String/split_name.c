#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int getStringCount(const char* text, char separator) {
    int initialPos = 0;
    int curr = 0;
    int numStrings = 0;
    int strLength = strlen(text);
    while (curr < strLength) {
        if (text[curr] == separator || curr == strLength - 1) {
            if (initialPos < curr) {
                numStrings ++;
            }
            initialPos = curr + 1;
        } 
        curr ++;
    }
    return numStrings;
}

char** stringSplit(const char* text, char separator, int* length) {
    int strLength = strlen(text);
    printf("%s %d \n", text, strLength);
    int subsStringsCount = getStringCount(text, separator);
    *length = subsStringsCount;
    char **substrings = malloc (sizeof (char *) * subsStringsCount);
    int indSubString = 0;
    printf("Num Strings %d \n", subsStringsCount);
    int initialPos = 0, curr = 0, numStrings = 0;
    while (curr <= strLength) {
        if (text[curr] == separator || curr == strLength) {
            if (initialPos < curr) {
                int substringSize = curr - initialPos; 
                printf("initial %d curr %d \n", initialPos, curr);
                printf("substring Size %d \n", substringSize);
                char* substring = malloc(sizeof(char) * substringSize);
                for (int i = 0; i < substringSize; i++) {
                    int textPos = initialPos + i;
                    substring[i] = text[textPos];
                }
                substrings[indSubString] = substring;
                printf("ind substring %d = %s \n", indSubString, substring);
                indSubString ++;
            }
            initialPos = curr + 1;
        } 
        curr ++;
    }
    return substrings;
}

int main (int argc, char* argv[]) {
    char* text = "/Fla/Flu/Bota/";
    int length;
    char ** substrings = stringSplit(text, '/', &length);
    for (int i = 0; i < length; i ++) {
        printf("%s \n", substrings[i]);
    }
}
#include <stdio.h>
#include <stdlib.h>

typedef struct stack Stack;
Stack *stackNew (void) {
    return 0;
}
void stackFree (Stack *p) {

}
void stackPush (Stack *p, int elem) {

}
int stackPop (Stack *p) {
    return 0;
}
int stackEmpty (Stack *p) {
    return 0;
}

typedef struct singlelinkedlist {
    int data; 
    struct singlelinkedlist * next;
} SingleLinkedListOfIntsNode;

void singleLinkedListFree(SingleLinkedListOfIntsNode* head) {
    
}

void listPartition(SingleLinkedListOfIntsNode **head, int x) {
    Stack* less = stackNew();
    Stack* greater = stackNew();
    Stack* elem = stackNew();
    SingleLinkedListOfIntsNode* curr = (*head);
    while (curr != 0) {
        if (curr->data < x) {
            stackPush(less, curr->data);
        } else if (curr->data > x) {
            stackPush(greater, curr->data);
        } else {
            stackPush(elem, curr->data);
        }
    }
    singleLinkedListFree(*head);
    SingleLinkedListOfIntsNode** newHead = (SingleLinkedListOfIntsNode**)malloc(sizeof(SingleLinkedListOfIntsNode*)); 
    curr = (SingleLinkedListOfIntsNode*)malloc(sizeof(SingleLinkedListOfIntsNode));
    *newHead = curr;
    while (stackEmpty(less) == 0) {
        curr->data = stackPop(less);
        curr->next = (SingleLinkedListOfIntsNode*)malloc(sizeof(SingleLinkedListOfIntsNode));
        curr = curr->next;
    }
    stackFree(less);
    while (stackEmpty(elem) == 0) {
        curr->data = stackPop(elem);
        curr->next = (SingleLinkedListOfIntsNode*)malloc(sizeof(SingleLinkedListOfIntsNode));
        curr = curr->next;
    }
    stackFree(elem);
    while (stackEmpty(greater) == 0) {
        curr->data = stackPop(greater);
        curr->next = (SingleLinkedListOfIntsNode*)malloc(sizeof(SingleLinkedListOfIntsNode));
        curr = curr->next;
    }
    free(curr->next);
    stackFree(greater);
    *head = *newHead;
    stackFree(less);
}

int main(int argc, char* argv[]) {

    return 0;
}
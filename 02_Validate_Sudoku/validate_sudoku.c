#include <stdio.h>
#include <stdlib.h>

int* validationArray() {
    int* validation = malloc(sizeof(int) * 9);
    for (int i = 0; i < 9; i ++) {
        validation[i] = 0;
    }
    return validation;
}

int isRowValid (int* row, int ncols) {
    int* validation = validationArray();
    for (int i = 0; i < ncols; i++) {
        int elem = row[i];
        if (validation[elem-1] != 0) {
            return 0;
        } else {
            validation[elem - 1] = 1;
        }
    }
    for (int i = 0; i < ncols; i++) {
        if (validation[i] == 0) {
            return 0;
        }
    }
    free(validation);
    return 1;
}

int isRowsValid (int nrows, int ncols, int sudoku[][9]) {
    for (int i = 0; i < nrows; i ++) {
        int rowValid = isRowValid(sudoku[i], ncols);
        if (rowValid == 0) {
            printf("Row %d \n", i);
            return 0;
        }
    }
    return 1;
}

int isColValid (int nrows, int selectedcol, int sudoku[][9]) {
    int* validation = validationArray();
    for (int i = 0; i < nrows; i++) {
        int elem = sudoku[i][selectedcol];
        if (validation[elem-1] != 0) {
            return 0;
        } else {
            validation[elem - 1] = 1;
        }
    }
    for (int i = 0; i < nrows; i++) {
        if (validation[i] == 0) {
            return 0;
        }
    }
    free(validation);
    return 1;
}

int isColsValid (int nrows, int ncols, int sudoku[][9]) {
    for (int i = 0; i < ncols; i++) {
        int colStatus = isColValid(nrows, i, sudoku);
        if (colStatus == 0) {
            printf("Col %d \n", i);
            return 0;
        }
    }
    return 1;
}

int isBlockValid(int rowIndex, int colIndex, int sudoku[][9]) {
    int* validation = validationArray();
    for (int i = rowIndex; i < (rowIndex + 3); i++ ) {
        for (int j = colIndex; j < (colIndex + 3); j++) {
            int elem = sudoku[i][j];
            if (validation[elem-1] != 0) {
                return 0;
            } else {
                validation[elem - 1] = 1;
            }
        }
    }
    for (int i = 0; i < 9; i++) {
        if (validation[i] == 0) {
            return 0;
        }
    }
    free(validation);
    return 1;
}

int isBlocksValid(int nrows, int ncols, int sudoku[][9]) {
    int status;
    for (int i = 0; i < nrows; i += 3) {
        for (int j = 0; j < ncols; j += 3) {
            status = isBlockValid(i, j, sudoku);
            if (status == 0) return status;
        }
    }
    return status;
}

int validateSudoku(int nrows, int ncols, int sudoku[][9]) {
    int status = 0;
    status = isRowsValid(nrows, ncols, sudoku);
    if (status == 0) return status; 
    status = isColsValid(nrows, ncols, sudoku);
    if (status == 0) return status; 
    status = isBlocksValid(nrows, ncols, sudoku);
    if (status == 0) return status; 
    return status;
}

void printMatrix (int nrows, int ncols, int sudoku[][9]) {
    for (int i = 0; i < nrows; i++) {
        for (int j = 0; j < ncols; j++) {
            printf("%d ", sudoku[i][j]);
        } 
        printf("\n");
    }
}

int main(int argc, char* argv[]) {
    int nrows = 9;
    int ncols = 9;
    int sudoku [9][9] = {
                            {5, 3, 4, 6, 7, 8, 9, 1, 2},
                            {6, 7, 2, 1, 9, 5, 2, 4, 8},
                            {1, 9, 8, 3, 4, 2, 5, 6, 7},
                            {8, 5, 9, 7, 6, 1, 4, 2, 3},
                            {4, 2, 6, 8, 5, 3, 7, 9, 1},
                            {7, 1, 3, 9, 2, 4, 8, 5, 6},
                            {9, 6, 1, 5, 3, 7, 2, 8, 4},                            
                            {2, 8, 7, 4, 1, 9, 6, 3, 5},
                            {3, 4, 5, 2, 8, 6, 1, 7, 9}
                        };
    printMatrix(nrows, ncols, sudoku);
    int isValidSudoku = isRowsValid(nrows, ncols, sudoku);
    if (isValidSudoku != 0) {
        printf("Sudoku is valid %d \n", isValidSudoku);
    } else {
        printf("Sudoku is not valid %d \n", isValidSudoku);
    }

    int sudoku2 [9][9] = {
                        {5, 3, 4, 6, 7, 8, 9, 1, 2},
                        {6, 7, 2, 1, 9, 5, 3, 4, 8},
                        {1, 9, 8, 3, 4, 2, 5, 6, 7},
                        {8, 5, 9, 7, 6, 1, 4, 2, 3},
                        {4, 2, 6, 8, 5, 3, 7, 9, 1},
                        {7, 1, 3, 9, 2, 4, 8, 5, 6},
                        {9, 6, 1, 5, 3, 7, 2, 8, 4},                            
                        {2, 8, 7, 4, 1, 9, 6, 3, 5},
                        {3, 4, 5, 2, 8, 6, 1, 7, 9}
                    };
    printMatrix(nrows, ncols, sudoku2);
    isValidSudoku = isColsValid(nrows, ncols, sudoku2);
    if (isValidSudoku != 0) {
        printf("Sudoku is valid %d \n", isValidSudoku);
    } else {
        printf("Sudoku is not valid %d \n", isValidSudoku);
    }

    int sudoku3 [9][9] = {
                        {5, 3, 4, 6, 7, 8, 9, 1, 2},
                        {6, 7, 2, 1, 9, 5, 3, 4, 8},
                        {1, 9, 8, 3, 4, 2, 5, 6, 7},
                        {8, 5, 9, 7, 6, 1, 4, 2, 3},
                        {4, 2, 6, 8, 5, 3, 7, 9, 1},
                        {7, 1, 3, 9, 2, 4, 8, 5, 6},
                        {9, 6, 1, 5, 3, 7, 2, 8, 4},                            
                        {2, 8, 7, 4, 1, 9, 6, 3, 5},
                        {3, 4, 5, 2, 8, 6, 1, 7, 9}
                    };
    printMatrix(nrows, ncols, sudoku3);
    isValidSudoku = isBlocksValid(nrows, ncols, sudoku3);
    if (isValidSudoku != 0) {
        printf("Sudoku is valid %d \n", isValidSudoku);
    } else {
        printf("Sudoku is not valid %d \n", isValidSudoku);
    }
    return 0;
}
#include <stdio.h>
#include <stdlib.h>

typedef char* err_t;

int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

void printList(int * list, int elemsCount) {
    for (int i = 0; i < elemsCount; i++) {
        printf("%d ", list[i]);
    }
    printf("\n");
}

err_t findCommonShortTall(int *small, int numSmall, int *tall, int numTall, int *result, int resultSize) {
    int indSmall = 0;
    int indTall = 0;
    int breakLoop = 0;
    int lastFound = 0;
    int indResult = 0;
    while (indSmall < numSmall && indTall < numTall && indResult < resultSize) {
        int smallElem = small[indSmall];
        int tallElem = tall[indTall];
        if (smallElem == tallElem) {
            if (smallElem != lastFound) {
                lastFound = smallElem;
                result[indResult++] = lastFound;
            } else {
                indSmall ++;
            }
        } else {
            indTall ++;
        }
    }
    if (indSmall < numSmall && indTall < numTall) return "Result space is over;";
    return "ok";
}

err_t findCommon(int *list1, int numElem1, int *list2, int numElem2, int *result, int resultSize) {
    qsort(list1, numElem1, sizeof(int), cmpfunc);
    printList(list1, numElem1);
    qsort(list2, numElem2, sizeof(int), cmpfunc);
    printList(list2, numElem2);
    if (numElem1 < numElem2) {
        return findCommonShortTall(list1, numElem1, list2, numElem2, result, resultSize);
    } 
    return findCommonShortTall(list2, numElem2, list1, numElem1, result, resultSize);
}

int main(int argc, char* argv[]) {
    int list1[] = {4, 2, 1, 3, 15, 6, 9};
    int list1Len = sizeof(list1) / sizeof(int);
    int list2[] = {10, 2, 30, 4, 5, 6, 7, 3, 20, 4, 1};
    int list2Len = sizeof(list2) / sizeof(int);
    int resultSize = (list1Len > list2Len) ? list1Len : list2Len;
    int* result = malloc(sizeof(int) * resultSize);
    err_t err = findCommon(list1, list1Len, list2, list2Len, result, resultSize);
    printf("Message %s \n", err);
    printf("Size %d \n", resultSize);
    printList(result, resultSize);
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "reversestring.h"

void test_empty_string() {
    // str vazia
    char emptystr[] = "";
    char* result = reversestring(emptystr);
    assert(strcmp("", result) == 0);
    printf("test_empty_string -- pass\n");
}

void test_unitary_string() {
    // str unitaria
    char a[] = "a";
    char* result = reversestring(a);
    assert(strcmp("a", result) == 0);
    printf("test_unitary_string -- pass\n");
}

void test_even_string_len() {
    char par[] = "ab";
    char* result = reversestring(par);
    assert(strcmp("ba", result) == 0);
    printf("test_even_string_len -- pass\n");
}

void test_odd_string_len() {
    char complete[] = "abc";
    char* result = reversestring(complete);
    assert(strcmp("cba", result) == 0);
    printf("test_odd_string_len -- pass\n");
}

int main (int argc, char* argv[]) {
    test_empty_string();
    test_unitary_string();
    test_even_string_len();
    test_odd_string_len();  
    return 0;
}
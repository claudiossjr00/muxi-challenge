#include "reversestring.h"

char* reversestring(char regular[]) {
    int length = strlen(regular);
    int first = 0;
    int last = length - 1;
    while (first < last) {
        char temp = regular[first];
        regular[first] = regular[last];
        regular[last] = temp;
        first ++;
        last --;
    }
    return regular;
}